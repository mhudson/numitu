package au.net.michaelhudson.numitu.prototype;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.csys.items.shapes.IColourRectangle;
import multiplicity3.input.IMultiTouchEventListener;
import multiplicity3.input.MultiTouchInputComponent;
import multiplicity3.input.events.MultiTouchCursorEvent;
import multiplicity3.input.events.MultiTouchObjectEvent;
import synergynet3.SynergyNetApp;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Michael Hudson on 10/05/2015.
 */
public class DragExample extends SynergyNetApp implements IMultiTouchEventListener {

    protected static Logger logger;

    public static void main(String[] args) {
        NETWORKING = false;
        logger = Logger.getLogger("log4j");
        MultiplicityClient client = MultiplicityClient.get();
        client.start();
        SynergyNetApp dragExample = new DragExample();
        client.setCurrentApp(dragExample);
        client.getContext().setTitle(dragExample.getFriendlyAppName());
    }

    @Override
    public String getFriendlyAppName() {
        return "Drag Example";
    }

    @Override
    public void shouldStart(MultiTouchInputComponent input, IQueueOwner iqo) {
        super.shouldStart(input, iqo);
        logger.info("Application Started");
        input.registerMultiTouchEventListener(this);
    }

    @Override
    public void cursorPressed(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void cursorReleased(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void cursorClicked(MultiTouchCursorEvent multiTouchCursorEvent) {
        addItem(createBox(120L, 60L), stage.tableToWorld(multiTouchCursorEvent.getPosition()));
    }

    @Override
    public void cursorChanged(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void objectAdded(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectRemoved(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectChanged(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    protected void addItem(IItem item, Vector2f loc) {
        stage.addItem(item);
        item.setWorldLocation(loc);
    }

    protected IColourRectangle createBox(float width, float height) {
        UUID id = UUID.randomUUID();
        try {
            IColourRectangle box = contentFactory.create(IColourRectangle.class, "box-".concat(id.toString()), id);
            box.setSize(width, height);
            box.setSolidBackgroundColour(ColorRGBA.randomColor());
            behaviourMaker.addBehaviour(box, RotateTranslateScaleBehaviour.class).setScaleLimits(1.0f, 3.0f);

            return box;
        } catch (ContentTypeNotBoundException e) {
            logger.warning(e.getMessage());
        }
        return null;
    }
}
