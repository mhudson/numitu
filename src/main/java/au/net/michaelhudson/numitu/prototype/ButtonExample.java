package au.net.michaelhudson.numitu.prototype;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.csys.items.mutablelabel.IMutableLabel;
import multiplicity3.csys.items.shapes.IColourRectangle;
import multiplicity3.input.IMultiTouchEventListener;
import multiplicity3.input.MultiTouchEventAdapter;
import multiplicity3.input.MultiTouchInputComponent;
import multiplicity3.input.events.MultiTouchCursorEvent;
import multiplicity3.input.events.MultiTouchObjectEvent;
import synergynet3.SynergyNetApp;
import synergynet3.additionalitems.interfaces.IButtonbox;
import synergynet3.fonts.FontColour;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Michael Hudson on 10/05/2015.
 */
public class ButtonExample extends SynergyNetApp implements IMultiTouchEventListener {

    protected static Logger logger;

    public static void main(String[] args) {
        NETWORKING = false;
        logger = Logger.getLogger("log4j");
        MultiplicityClient client = MultiplicityClient.get();
        client.start();
        SynergyNetApp buttonExample = new ButtonExample();
        client.setCurrentApp(buttonExample);
        client.getContext().setTitle(buttonExample.getFriendlyAppName());
    }

    @Override
    public String getFriendlyAppName() {
        return "Button Example";
    }

    @Override
    public void shouldStart(MultiTouchInputComponent input, IQueueOwner iqo) {
        super.shouldStart(input, iqo);
        logger.info("Application Started");
        input.registerMultiTouchEventListener(this);
        addMenu();
    }

    @Override
    public void cursorPressed(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void cursorReleased(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void cursorClicked(MultiTouchCursorEvent multiTouchCursorEvent) {

    }

    @Override
    public void cursorChanged(MultiTouchCursorEvent multiTouchCursorEvent) {
    }

    @Override
    public void objectAdded(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectRemoved(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectChanged(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    protected void addItem(IItem item, float xpos, float ypos) {
        addItem(item, new Vector2f(xpos, ypos));
    }

    protected void addItem(IItem item, Vector2f loc) {
        stage.addItem(item);
        item.setWorldLocation(loc);
    }

    protected IColourRectangle createBox(float width, float height) {
        UUID id = UUID.randomUUID();
        try {
            IColourRectangle box = contentFactory.create(IColourRectangle.class, "box-".concat(id.toString()), id);
            box.setSize(width, height);
            box.setSolidBackgroundColour(ColorRGBA.randomColor());
            behaviourMaker.addBehaviour(box, RotateTranslateScaleBehaviour.class).setScaleLimits(1.0f, 3.0f);
            return box;
        } catch (ContentTypeNotBoundException e) {
            logger.warning(e.getMessage());
        }
        return null;
    }

    protected IMutableLabel createLabel(String text) {
        UUID id = UUID.randomUUID();
        try {
            IMutableLabel label = contentFactory.create(IMutableLabel.class, "label-".concat(id.toString()), id);
            label.setText(text);
        } catch (ContentTypeNotBoundException e) {
            logger.warning(e.getMessage());
        }
        return null;
    }

    private void addMenu() {
        IButtonbox button;
        try {
            button = contentFactory.create(IButtonbox.class, "button", UUID.randomUUID());
            button.setText("Add Item", ColorRGBA.Blue, ColorRGBA.Cyan, FontColour.Orange, 200f, 120f, stage);
            addItem(button, 150f, 150f);
            button.getListener().getMultiTouchDispatcher().addListener(new MultiTouchEventAdapter() {
                @Override
                public void cursorClicked(MultiTouchCursorEvent event){
                    addItem(createBox(50f, 50f), 300f, 300f);
                }
            });
        } catch (ContentTypeNotBoundException e) {
            logger.warning(e.getMessage());
        }
        IMutableLabel label = createLabel("Add Item");
    }
}
